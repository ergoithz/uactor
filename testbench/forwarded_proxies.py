import uactor

class MyActor(uactor.Actor):
    _exposed_ = ('my_other_actor',)

    def __init__(self):
        self.my_other_actor = MyOtherActor()

class MyOtherActor(uactor.Actor):
    _options_ = {'address': ('0.0.0.0', 7000), 'authkey': b'OtherSecret'}

with MyActor() as actor:
    try:
        my_other_actor = actor.my_other_actor
        raise RuntimeError('AuthkeyError not raised')
    except uactor.AuthkeyError:
        pass
    address = 'localhost', 7000
    capture = [(('0.0.0.0'), 7000)]
    with MyOtherActor.connect(address, b'OtherSecret', capture=capture):
        my_other_actor = actor.my_other_actor
        print(my_other_actor.connection_address)
