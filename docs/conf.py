"""
Configuration file for the Sphinx documentation builder.

Reference: https://www.sphinx-doc.org/en/master/usage/configuration.html

"""

import uactor

project = uactor.__name__
copyright = f'2020, {uactor.__author__}'  # noqa
author = uactor.__author__
release = uactor.__version__
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinx.ext.coverage',
    'sphinx.ext.intersphinx',
    'sphinx.ext.autosectionlabel',
    'recommonmark',
    ]
templates_path = ['_templates']
exclude_patterns = []
html_theme = 'alabaster'
html_static_path = ['_static']
html_sidebars = {
    '**': [
        'sidebar.html',
        'globaltoc.html',
        'relations.html',
        'sourcelink.html',
        'searchbox.html',
        ],
    }
source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown',
    '.txt': 'markdown',
    }
autodoc_default_options = {
    'members': True,
    'undoc-members': False,
    'no-undoc-members': True,
    'private-members': ','.join((
        '_options_',
        '_proxies_',
        '_exposed_',
        '_method_to_typeid_',
        )),
    'special-members': ','.join((
        '__enter__',
        '__exit__',
        )),
    'inherited-members': False,
    'show-inheritance': True,
    'member-order': 'bysource',
    'exclude-members': ','.join((
        '__init_subclass__',
        '__weakref__',
        '_callmethod',
        '_manager',
        '_Server',
        )),
    }
intersphinx_mapping = {
    'python': ('https://docs.python.org/', None),
    }
autosectionlabel_prefix_document = True
