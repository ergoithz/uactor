.. title:: Index

Documentation
=============

Welcome to uActor documentation.

uActor: Process Actor Model
---------------------------

uActor is a process actor library for Python with a simple yet powerful API,
implementing the `actor model`_ atop :mod:`multiprocessing`,
with no dependencies other than the `Python Standard Library`_.

.. _actor-model: https://en.wikipedia.org/wiki/Actor_model
.. _stdlib: https://docs.python.org/3/library/index.html

.. _Python Standard Library: stdlib_
.. _actor model: actor-model_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Readme <README>
   API reference <uactor>
   License <LICENSE>

.. toctree::
   :maxdepth: 2
   :caption: Examples:

   examples/inheritance
   examples/lifetime
   examples/result_proxies
   examples/callbacks

.. toctree::
   :maxdepth: 2
   :caption: Advanced patterns:

   examples/stick
   examples/pool
   examples/networking

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
